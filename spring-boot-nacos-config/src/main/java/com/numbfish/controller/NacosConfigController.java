package com.numbfish.controller;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
// 加载dataId 为example 的配置源 autoRefreshed = true 开启自动更新
@NacosPropertySource(dataId = "example",autoRefreshed = true)
@RestController
public class NacosConfigController {
    // @NacosValue 设置属性值 info 表示key， Local Hello World 表示默认值
    @NacosValue(value = "${info}",autoRefreshed = true)
    private String info;

    @GetMapping("/config")
    public String get(){
        System.out.println(info);
        return info;
    }
}
