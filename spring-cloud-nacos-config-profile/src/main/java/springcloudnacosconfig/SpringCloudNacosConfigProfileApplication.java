package springcloudnacosconfig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SpringCloudNacosConfigProfileApplication {

	public static void main(String[] args) throws Exception{
		ConfigurableApplicationContext context=
				SpringApplication.run(SpringCloudNacosConfigProfileApplication.class, args);
		while (true){
			String info=context.getEnvironment().getProperty("info");
			System.out.println(info);
			Thread.sleep(2000);
		}
	}
}
